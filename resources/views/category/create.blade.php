@include('layouts.header')
<div class="container mt-5">
    <div class="card" style="width: 30%">
        <div class="card-header">
            Tambah Kategori
        </div>

        <div class="card-body">
            <form action="{{ route('categories.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Kategori</label>
                    <input type="text" class="form-control" name="name" id="name">
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


@include('layouts.footer')
