@include('layouts.header')

<div class="container mt-5">
    <div class="card">
        <div class="card-header">
            Kategori
        </div>
        @if (session()->has('success'))
            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif (session()->has('success-edit'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('success-edit') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif(session()->has('success-delete'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('success-delete') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="card-body">
            <a href="categories/create" class="btn btn-primary mb-3">Tambah Kategori</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" style="width: 10%">#</th>
                        <th scope="col" style="width: 30%">Nama Kategori</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($category as $c)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $c->name }}</td>
                            <td>
                                <div class="btn-group">
                                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                        <a href="{{ route('categories.edit', $c->id) }}" type="button"
                                            class="btn btn-success btn-sm">
                                            <i class="fa-solid fa-pencil"></i> Edit
                                        </a>

                                        <form action="{{ route('categories.destroy', $c->id) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="submit"
                                                onclick="return confirm('Apakah anda yakin akan menghapus data ?') "
                                                class="btn btn-danger btn-sm"><i class="fa-solid fa-trash-can"></i>
                                                Hapus</button>

                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@include('layouts.footer')
