@include('layouts.header')
<div class="container mt-5">
    <div class="card" style="width: 30%">
        <div class="card-header">
            Tambah Uang Masuk
        </div>

        <div class="card-body">
            <form action="{{ route('cashflow.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="category_id">Kategori</label>
                    <select class="form-control" name="category_id" id="category_id">
                        <option selected value="" disabled> &mdash; Pilih &mdash; </option>
                        @foreach ($category as $c)
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nominal">Nominal</label>
                    <input type="text" class="form-control" name="nominal" id="nominal">
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


@include('layouts.footer')
