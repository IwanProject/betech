<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/styles/css/themes/lite-purple.min.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/logo.png') }}">
</head>

<body class="text-left">
    <div class="auth-layout-wrap" style="background-color: orange">
        <div class="auth-content">

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="p-4">
                                @if (session()->has('success'))
                                    <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                        {{ session('success') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @elseif (session()->has('success-edit'))
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        {{ session('success-edit') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @elseif(session()->has('success-delete'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ session('success-delete') }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @endif
                                <h1 class="mb-3 text-18">Sign In</h1>
                                <form action="/login" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input id="email" autocomplete="off" name="email"
                                            class="form-control form-control-rounded" type="email"
                                            value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" autocomplete="off" name="password"
                                            class="form-control form-control-rounded" type="password">
                                    </div>
                                    <button type="submit"
                                        class="btn btn-rounded btn-primary btn-block mt-2 btn-outline-dark"
                                        style="background-color:orange;">Sign
                                        In</button>

                                </form>
                                <a href="/register">Belum punya akun?</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

    <script src="{{ asset('assets/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/es5/script.min.js') }}"></script>
</body>

</html>
