<?php

namespace App\Models;

use App\Models\Cashflow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];

    function cashflow()
    {
        return $this->hasMany(Cashflow::class);
    }
}
