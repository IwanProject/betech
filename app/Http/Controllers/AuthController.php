<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.index', ['title' => 'Login']);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }


        return redirect('/login')->with('success', 'Email atau Password salah!');
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }

    public function register()
    {
        return view('auth.register', ['title' => 'Register']);
    }

    public function registration(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);

        $validate['password'] = bcrypt($request->password);

        try {
            User::create($validate);
            return redirect('/login')->with('success', 'Register Success!');
        } catch (Exception $e) {
            return redirect('/register')->with('success', 'Gagal Success!');
        }
    }
}
