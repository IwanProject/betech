<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Cashflow;
use App\Models\Category;
use Illuminate\Http\Request;

class CashflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cashflow.index', ['title' => 'Daftar Transaksi', 'cashflow' => Cashflow::with('category')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cashflow.create', ['title' => 'Transaksi', 'category' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'nominal' => 'required'
        ]);

        $validate['category_id'] = $request->category_id;
        try {
            Cashflow::create($validate);
            return redirect('/cashflow')->with('success', 'Data berhasil ditambahkan!');
        } catch (Exception $e) {
            return redirect('/cashflow')->with('success', 'Data gagal ditambahkan!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cashflow  $cashflow
     * @return \Illuminate\Http\Response
     */
    public function show(Cashflow $cashflow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cashflow  $cashflow
     * @return \Illuminate\Http\Response
     */
    public function edit(Cashflow $cashflow)
    {
        return view('cashflow.edit', ['title' => 'Edit Transaksi', 'category' => Category::all(), 'cashflow' => $cashflow]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cashflow  $cashflow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cashflow $cashflow)
    {
        $validate = $request->validate([
            'nominal' => 'required'
        ]);

        $validate['category_id'] = $request->category_id;
        try {
            Cashflow::where('id', $cashflow->id)->update($validate);
            return redirect('/cashflow')->with('success', 'Data berhasil diupdate!');
        } catch (Exception $e) {
            return redirect('/cashflow')->with('success', 'Data gagal diupdate!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cashflow  $cashflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cashflow $cashflow)
    {
        Cashflow::destroy($cashflow->id);
        return redirect('/cashflow')->with('success-delete', 'Data berhasil dihapus!');
    }
}
